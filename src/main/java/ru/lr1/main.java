package ru.lr1;

import org.springframework.context.ApplicationContext;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import ru.lr1.Service.OneServ;
import ru.lr1.Service.OneServImpl;

import java.io.IOException;

public class main {

    public static void main(String [] args) throws IOException {
        ApplicationContext context = new ClassPathXmlApplicationContext("bean.xml");
        OneServ service = context.getBean(OneServ.class);
        service.run();
    }
}
