package ru.lr1.DAOClass;

import au.com.bytecode.opencsv.CSVReader;
import ru.lr1.Entity.Answer;
import ru.lr1.Entity.Quest;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class DAOQuestImp implements DAOQuest {


    @Override
    public List<Quest> ListQuest() throws IOException {
        List<Quest> List = new ArrayList<Quest>();
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("Quest.csv").getFile());
            CSVReader reader = new CSVReader(new FileReader(file));
            String[] nextLine;
            while ((nextLine = reader.readNext()) != null) {
                if (nextLine != null) {
                    List<Answer> answers = new ArrayList<Answer>();
                    for (int i =2; i<nextLine.length; i++){
                        Answer answer = new Answer();
                        answer.setName(nextLine[i]);
                        answers.add(answer);
                    }
                    Quest quest = new Quest(nextLine[0], answers, nextLine[1]);
                    List.add(quest);
                }
            }
            reader.close();
        return List;
        }
    }
