package ru.lr1.Service;

import ru.lr1.DAOClass.DAOQuest;
import ru.lr1.DAOClass.DAOQuestImp;
import ru.lr1.Entity.Answer;
import ru.lr1.Entity.Quest;

import java.io.IOException;
import java.util.List;
import java.util.Scanner;

import static java.lang.System.in;


public class OneServImpl implements OneServ {

    private final DAOQuest questsDAO;

    public OneServImpl(DAOQuest questsDAO) {
        this.questsDAO = questsDAO;
    }


    public boolean AnswerUser(Quest quest){
        System.out.println("Выберите вариант ответа");
        try{
            Scanner scanner = new Scanner(in);
            int numberAnswer = scanner.nextInt();
            if(numberAnswer <= quest.getListAnswer().size()){
                if(quest.getListAnswer().get(numberAnswer-1).getName().equals(quest.getTrueAnswer())){
                    System.out.println("Верный ответ!");
                    return true;
                }else{
                    System.out.println("Неверный ответ!");
                    return false;
                }
            }
        }catch (Exception e) {
            System.out.println("Ответа нет или написан некорректно");
        }
        return true;
    }


    public void run() throws IOException {
        int TrueAnswer =0;
        List<Quest> list = questsDAO.ListQuest();
            for(Quest quest:list){
                System.out.println(quest.getName());
                int countAnswer=1;
                for(Answer answer:quest.getListAnswer()){
                    System.out.println(countAnswer+". "+answer.getName());
                    countAnswer++;
                }
                boolean answer = AnswerUser(quest);
                if(answer) {
                    TrueAnswer++;
                }
            }
            System.out.println("Правильных ответов "+TrueAnswer+" из "+list.size());
        }
    }

